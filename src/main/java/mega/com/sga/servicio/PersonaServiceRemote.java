package mega.com.sga.servicio;

import java.util.List;

import javax.ejb.Remote;
import mega.com.sga.domain.Persona;;

@Remote
public interface PersonaServiceRemote {
	
	public List<Persona> listarPersonas();

	public Persona encontrarPersonaPorId(Persona persona);
	
	public Persona encontrarPersonaPorEmail(String email);
	
	public void registrarPersona(Persona persona);
	public void modificarPersona(Persona persona);
	public void eliminarPersona(Persona persona);
}
