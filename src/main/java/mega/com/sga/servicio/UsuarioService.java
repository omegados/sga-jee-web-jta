package mega.com.sga.servicio;

import java.util.List;

import javax.ejb.Local;

import mega.com.sga.domain.Usuario;

@Local
public interface UsuarioService {
	public List<Usuario> listarUsuarios();

	public Usuario encontrarUsuarioPorId(int idUsuario);
	
	public void registrarUsuario(Usuario usuario);
	public void modificarUsuario(Usuario usuario);
	public void eliminarUsuario(Usuario usuario);
}
