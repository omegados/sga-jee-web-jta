package mega.com.sga.web;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mega.com.sga.domain.Persona;
import mega.com.sga.servicio.PersonaService;

@WebServlet("/personas")
public class PersonaServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	PersonaService personaService;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Persona> personas = personaService.listarPersonas();
		System.out.println("Personas: " + personas);
		request.setAttribute("personas", personas);
		request.getRequestDispatcher("/listadoPersonas.jsp").forward(request, response);
	}
	

}
