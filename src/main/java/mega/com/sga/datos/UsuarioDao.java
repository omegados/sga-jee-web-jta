package mega.com.sga.datos;

import java.util.List;

import mega.com.sga.domain.Usuario;

public interface UsuarioDao {

		public List<Usuario> findAllUsuarios();
		
		public Usuario findUsuarioById(int idUsuario);
			
		public void insertUsuario(Usuario usuario);
		
		public void updateUsuario(Usuario usuario);
		
		public void deleteUsuario(Usuario usuario);



}
