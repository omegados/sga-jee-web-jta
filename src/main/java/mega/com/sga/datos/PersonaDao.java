package mega.com.sga.datos;

import java.util.List;

import mega.com.sga.domain.Persona;

public interface PersonaDao {
	public List<Persona> findAllPersonas();
	
	public Persona findPersonaById(Persona persona);
	
	public Persona findPersonaByEmail(String email);
	
	public void insertPersona(Persona persona);
	
	public void updatePersona(Persona persona);
	
	public void deletePersona(Persona persona);

}
