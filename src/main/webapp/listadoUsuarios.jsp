<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Listado de Usuarios</title>
</head>
<body>
	<h1>Listado de Usuarios</h1>
	<ul>
		<c:forEach items="${usuarios}" var="usuarios">
			<li>Username: ${usuarios.username} <br />Password: ${usuarios.password} <br />Persona: ${usuarios.persona}</li>
		</c:forEach>
	</ul>
</body>
</html>